import express from "express";
import bodyParser from "body-parser";
import KcAdminClient from "keycloak-admin";

const app = express();
const port = 3000;
const kcAdminClient = new KcAdminClient();

app.use(bodyParser.json());

app.get("/", async (req, res) => {
  return res.send("Test");
});

app.post("/token", async (req, res) => {
  try {
    const { username, password } = req.body;
    await kcAdminClient.auth({
      username,
      password,
      grantType: "password",
      clientId: "admin-cli",
    });
    const token = await kcAdminClient.getAccessToken();
    return res.json({ token });
  } catch (error) {
    return res.json({ error: error.toString() });
  }
});

app.post("/users", async (req, res) => {
  try {
    const newUser = await kcAdminClient.users.create(req.body);

    return res.json({ user: newUser });
  } catch (error) {
    return res.json({ error: error.toString() });
  }
});

app.get("/users", async (req, res) => {
  try {
    const users = await kcAdminClient.users.find();

    return res.json({ users });
  } catch (error) {
    return res.json({ error: error.toString() });
  }
});

app.put("/users/:id/password", async (req, res) => {
  try {
    const { id } = req.params;
    const { password } = req.body;
    await kcAdminClient.users.resetPassword({
      id,
      credential: { value: password },
    });

    return res.json({ success: true });
  } catch (error) {
    return res.json({ error: error.toString() });
  }
});

app.post("/clients", async (req, res) => {
  try {
    const {
      clientInfo: { clientId, rootUrl, redirectUris },
    } = req.body;
    const newClient = await kcAdminClient.clients.create({
      clientId,
      rootUrl,
      redirectUris,
      publicClient: true,
      consentRequired: true,
    });
    return res.json({ client: newClient });
  } catch (error) {
    return res.json({ error: error.toString() });
  }
});

app.get("/clients/:clientId", async (req, res) => {
  try {
    const clientId = req.params.clientId;
    const client = await kcAdminClient.clients.findOne({
      id: clientId,
    });

    return res.json({ client });
  } catch (error) {
    return res.json({ error: error.toString() });
  }
});

app.listen(port, () => console.log(`app listening at ${port}`));
