# Demo

## Install and setup Keycloak 
- Clone the repository
- Import `realm-export.json`
- Unzip `demo-theme.zip` and copy the folder to the Keycloak theme directory `keycloak-10.0.2/themes`
- Install and run Keycloak
- Set new theme for login page in realm setting
  ![theme setting](images/theme-setting.png)
- Run Keycloak

## Install and run frontend and backend
- Run `npm install` in the `root`, `frontend` and `frontend-oidc` directory
- Run `npm run serve` in `frontend` and `frontend-oidc` to start 
- Run `npm run watch:ts` and `npm start` to in `root` to start backend server